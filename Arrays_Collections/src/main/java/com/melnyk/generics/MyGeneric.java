package com.melnyk.generics;

import com.melnyk.generics.laptops.Laptop;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyGeneric {

  private List<Laptop> myLaptops = new ArrayList<Laptop>();

  public void putLaptops(List<? extends Laptop> laptops) {
    for (Laptop laptop : laptops) {
      Collections.addAll(myLaptops, laptop);
    }
  }

  public void getLaptops(List<? super Laptop> laptops) {
    for (Laptop laptop : myLaptops) {
      Collections.addAll(laptops, laptop);
    }
  }
}
