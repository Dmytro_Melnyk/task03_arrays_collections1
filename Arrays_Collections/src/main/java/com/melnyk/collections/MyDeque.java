package com.melnyk.collections;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyDeque<E> implements Iterable<E> {

  private Object[] queue;

  private int size;

  private int hasNextIndex = -1;

  private int length = 10;

  public Iterator<E> iterator() {
    hasNextIndex = -1;
    return new Iterator<E>() {

      public boolean hasNext() {
        if ((hasNextIndex < queue.length - 1) && (queue[hasNextIndex + 1]
            != null)) {
          hasNextIndex++;
          return true;
        } else {
          return false;
        }
      }

      @Override
      public void remove() {

      }

      public E next() {
        return (E) queue[hasNextIndex];
      }
    };

  }

  public MyDeque() {
    queue = new Object[length];
  }


  public void addFirst(E e) {
    if (size < queue.length) {
      System.arraycopy(queue, 0, queue, 1, size);
      queue[0] = e;
      size++;
    } else {
      queue = copy();
      addFirst(e);
    }
  }

  public void addLast(E e) {
    if (size < queue.length) {
      queue[size] = e;
      size++;
    } else {
      queue = copy();
      addLast(e);
    }
  }

  public boolean offerFirst(E e) {
    addFirst(e);
    return true;
  }

  public boolean offerLast(E e) {
    addLast(e);
    return true;
  }

  private Object[] copy() {
    length += 5;
    Object[] temp = new Object[length];
    System.arraycopy(queue, 0, temp, 0, queue.length);
    return temp;
  }

  public void clear() {
    int size = this.size;
    for (int i = 0; i < size; i++) {
      queue[i] = null;
      this.size--;
    }
  }

  public Object[] toArray() {
    if (size != 0) {
      Object[] temp = new Object[size];
      System.arraycopy(queue, 0, temp, 0, temp.length);
      return temp;
    } else {
      return new Object[size];
    }
  }

  public E peekFirst() {
    if (size != 0) {
      return (E) queue[0];
    } else {
      return null;
    }
  }

  public E peekLast() {
    if (size != 0) {
      return (E) queue[size - 1];
    } else {
      return null;
    }
  }

  public E getFirst() {
    if (size != 0) {
      return (E) queue[0];
    } else {
      throw new NoSuchElementException();
    }
  }

  public E getLast() {
    if (size != 0) {
      return (E) queue[size - 1];
    } else {
      throw new NoSuchElementException();
    }
  }


  public E pollFirst() {
    if (size != 0) {
      E element = (E) queue[0];
      size--;
      System.arraycopy(queue, 1, queue, 0, size);
      return element;
    } else {
      return null;
    }
  }

  public E pollLast() {
    if (size != 0) {
      E element = (E) queue[size - 1];
      queue[size - 1] = null;
      size--;
      return element;
    } else {
      return null;
    }
  }

  public E removeFirst() {
    if (size != 0) {
      E element = (E) queue[0];
      size--;
      System.arraycopy(queue, 1, queue, 0, size);
      return element;
    } else {
      throw new NoSuchElementException();
    }
  }

  public E removeLast() {
    if (size != 0) {
      E element = (E) queue[size - 1];
      queue[size - 1] = null;
      size--;
      return element;
    } else {
      throw new NoSuchElementException();
    }
  }

  public int size() {
    return size;
  }
}
