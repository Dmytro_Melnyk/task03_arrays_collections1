package com.melnyk.arrays;

public class TaskB {

  private int[] array;

  public TaskB(int[] array) {
    this.array = array;
  }

  public void getArrayWithoutDuplicateNumbers() {
    int[] someArray = new int[array.length];
    int arraySize = 0;
    boolean checker = true;

    for (int i = 0; i < array.length; i++) {
      for (int j = i + 1; j < array.length; j++) {
        if (array[i] == array[j]) {
          checker = false;
          break;
        }
      }
      if (checker) {
        someArray[arraySize++] = array[i];
      }
      checker = true;
    }

    int[] arrayWithoutDuplicate = new int[arraySize];
    System.arraycopy(someArray, 0, arrayWithoutDuplicate, 0, arraySize);

    for (int number : arrayWithoutDuplicate) {
      System.out.print(number + " ");
    }
    System.out.println();
  }
}
