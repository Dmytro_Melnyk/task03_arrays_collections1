package com.melnyk.arrays;

public class TaskC {

  private int[] array;

  public TaskC(int[] array) {
    this.array = array;
  }

  public void getArrayWithoutNumbersWhichAreDuplicate() {
    int[] someArray = new int[array.length];
    int arraySize = 0;

    for (int i = 0; i < array.length - 1; i++) {
      if (array[i] != array[i + 1]) {
        someArray[arraySize++] = array[i];
      }
      if (i + 1 == array.length - 1 && array[i] != array[i + 1]) {
        someArray[arraySize++] = array[i + 1];
      }
    }

    int[] arrayWithoutDublicate = new int[arraySize];
    System.arraycopy(someArray, 0, arrayWithoutDublicate, 0, arraySize);

    for (int number : arrayWithoutDublicate) {
      System.out.print(number + " ");
    }
    System.out.println(arraySize);
  }
}
