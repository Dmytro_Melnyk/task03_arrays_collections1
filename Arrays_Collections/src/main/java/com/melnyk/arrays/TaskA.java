package com.melnyk.arrays;

public class TaskA {

  private int[] array1;
  private int[] array2;

  public TaskA(int[] array1, int[] array2) {
    this.array1 = array1;
    this.array2 = array2;
  }

  public void getArrayWithDuplicateValues() {
    int[] array = new int[array1.length];
    int arraySize = 0;
    boolean checker = false;
    for (int anArray1 : array1) {
      for (int anArray2 : array2) {
        if (anArray1 == anArray2) {
          checker = true;
          break;
        }
      }
      if (checker) {
        for (int anArray3 : array) {
          if (anArray1 == anArray3) {
            checker = false;
            break;
          }
        }
      }
      if (checker) {
        array[arraySize] = anArray1;
        arraySize++;
      }
      checker = false;
    }

    int[] array3 = new int[arraySize];
    System.arraycopy(array, 0, array3, 0, arraySize);

    for (int number : array3) {
      System.out.print(number + " ");
    }
    System.out.println();
  }

  public void getArrayWithUniqueValues() {
    int[] array = new int[array1.length + array2.length];
    int arraySize = 0;
    boolean checker = true;
    for (int anArray1 : array1) {
      for (int anArray2 : array2) {
        if (anArray1 == anArray2) {
          checker = false;
          break;
        }
      }
      if (checker) {
        array[arraySize] = anArray1;
        arraySize++;
      }
      checker = true;
    }
    for (int anArray2 : array2) {
      for (int anArray1 : array1) {
        if (anArray2 == anArray1) {
          checker = false;
          break;
        }
      }
      if (checker) {
        array[arraySize++] = anArray2;
      }
      checker = true;
    }

    int[] array3 = new int[arraySize];
    System.arraycopy(array, 0, array3, 0, arraySize);

    for (int number : array3) {
      System.out.print(number + " ");
    }
    System.out.println();
  }
}
