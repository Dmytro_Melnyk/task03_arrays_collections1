package com.melnyk;

import com.melnyk.arrays.TaskA;
import com.melnyk.arrays.TaskB;
import com.melnyk.arrays.TaskC;
import java.util.Objects;

public class Main {

  String s;
  public static void main(String[] args) {
    int[] array1 = {0, 1, 4, 4, 62, 62, 4, 5, 8};
    int[] array2 = {1, 5, 4, 21, 4, 51, 5, 62};
    TaskA taskA = new TaskA(array1, array2);
    taskA.getArrayWithDuplicateValues();
    taskA.getArrayWithUniqueValues();
    TaskB taskB = new TaskB(array1);
    taskB.getArrayWithoutDuplicateNumbers();
    TaskC taskC = new TaskC(array1);
    taskC.getArrayWithoutNumbersWhichAreDuplicate();

    TaskA taskA1 = new TaskA(array1, array2);

    System.out.println(taskA.hashCode() == taskA1.hashCode());
    System.out.println(taskA.equals(taskA1));


  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Main)) {
      return false;
    }
    Main main = (Main) o;
    return Objects.equals(s, main.s);
  }

  @Override
  public int hashCode() {
    return Objects.hash(s);
  }
}
